package org.pak.trackerapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.pak.trackerapi.model.dto.RunDTO;
import org.pak.trackerapi.model.dto.RunFinishDTO;
import org.pak.trackerapi.model.dto.RunStartCreateDTO;
import org.pak.trackerapi.model.dto.UserStatisticsDTO;
import org.pak.trackerapi.service.RunService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;
import java.util.UUID;

@RestController
@RequestMapping("/api/runs")
@RequiredArgsConstructor
public class RunController {

    private final RunService runService;

    @Operation(summary = "Start a run")
    @PostMapping("/start")
    public ResponseEntity<RunStartCreateDTO> startRun(@Valid @RequestBody RunStartCreateDTO runStartCreateDTO) {
        return new ResponseEntity<>(runService.startRun(runStartCreateDTO), HttpStatus.CREATED);
    }

    @Operation(summary = "Finish a run")
    @PostMapping("/finish")
    public ResponseEntity<RunDTO> finishRun(@Valid @RequestBody RunFinishDTO runFinishDTO) {
        return ResponseEntity.ok(runService.finishRun(runFinishDTO));
    }

    @Operation(summary = "Get statistics for a user within a date range")
    @GetMapping("/user/{userId}/statistics")
    public ResponseEntity<UserStatisticsDTO> getUserStatistics(
            @PathVariable UUID userId,
            @RequestParam(required = false) OffsetDateTime fromDateTime,
            @RequestParam(required = false) OffsetDateTime toDateTime) {
        UserStatisticsDTO statistics = runService.getUserStatistics(userId, fromDateTime, toDateTime);
        return ResponseEntity.ok(statistics);
    }

    @Operation(summary = "Get runs for a user within a date range with pagination")
    @GetMapping("/user/{userId}")
    public Page<RunDTO> getRunsForUserWithPagination(
            @PathVariable UUID userId,
            @RequestParam(required = false) OffsetDateTime fromDateTime,
            @RequestParam(required = false) OffsetDateTime toDateTime,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        return runService.getRunsForUserWithPagination(userId, fromDateTime, toDateTime, PageRequest.of(page, size));
    }
}
