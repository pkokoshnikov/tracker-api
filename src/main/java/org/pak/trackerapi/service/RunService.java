package org.pak.trackerapi.service;

import lombok.RequiredArgsConstructor;
import org.pak.trackerapi.converter.RunDTOConverter;
import org.pak.trackerapi.converter.RunStartDTOConverter;
import org.pak.trackerapi.error.BusinessException;
import org.pak.trackerapi.model.dto.RunDTO;
import org.pak.trackerapi.model.dto.RunFinishDTO;
import org.pak.trackerapi.model.dto.RunStartCreateDTO;
import org.pak.trackerapi.model.dto.UserStatisticsDTO;
import org.pak.trackerapi.model.entity.RunEntity;
import org.pak.trackerapi.repository.RunRepository;
import org.pak.trackerapi.repository.UserRepository;
import org.pak.trackerapi.util.Calculator;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RunService {

    private final RunRepository runRepository;
    private final UserRepository userRepository;
    private final Calculator calculator;

    public UserStatisticsDTO getUserStatistics(UUID userId, OffsetDateTime fromDateTime, OffsetDateTime toDateTime) {
        if (!userRepository.existsById(userId)) {
            throw new BusinessException("User not found");
        }
        List<RunEntity> runs;
        if (fromDateTime != null && toDateTime != null) {
            runs = runRepository.findByUserIdAndStartDateTimeBetween(userId, fromDateTime, toDateTime);
        } else if (fromDateTime != null) {
            runs = runRepository.findByUserIdAndStartDateTimeGreaterThanEqual(userId, fromDateTime);
        } else if (toDateTime != null) {
            runs = runRepository.findByUserIdAndStartDateTimeLessThanEqual(userId, toDateTime);
        } else {
            runs = runRepository.findByUserId(userId);
        }

        long totalRuns = runs.size();
        long totalDistance = runs.stream()
                .mapToLong(RunEntity::getDistance)
                .sum();

        double averageSpeed = calculator.averageSpeed(runs.stream()
                .map(r -> new Calculator.DistanceTimePair(r.getDistance(),
                        r.getFinishDateTime().getEpochSecond() - r.getStartDateTime().getEpochSecond()))
                .toArray(Calculator.DistanceTimePair[]::new));

        return new UserStatisticsDTO(userId, totalRuns, totalDistance, averageSpeed);
    }

    public RunStartCreateDTO startRun(RunStartCreateDTO runStartCreateDTO) {
        if (!userRepository.existsById(runStartCreateDTO.getUserId())) {
            throw new BusinessException("User not found");
        }

        try {
            return RunStartDTOConverter.fromEntity(
                    runRepository.insert(RunStartDTOConverter.toEntity(runStartCreateDTO)));
        } catch (DuplicateKeyException e) {
            throw new BusinessException("Run already exists");
        }
    }

    public RunDTO finishRun(RunFinishDTO runFinishDTO) {
        if (!userRepository.existsById(runFinishDTO.getUserId())) {
            throw new BusinessException("User not found");
        }

        var startedRun = runRepository.findByIdAndUserId(runFinishDTO.getId(), runFinishDTO.getUserId());

        if (startedRun != null) {
            startedRun.setFinishLatitude(runFinishDTO.getFinishLatitude());
            startedRun.setFinishLongitude(runFinishDTO.getFinishLongitude());
            startedRun.setFinishDateTime(runFinishDTO.getFinishDateTime().toInstant());
            if (runFinishDTO.getDistance() == null) {
                startedRun.setDistance(calculator.distance(startedRun.getStartLatitude(),
                        startedRun.getStartLongitude(),
                        startedRun.getFinishLatitude(),
                        startedRun.getFinishLongitude()));
            } else {
                startedRun.setDistance(runFinishDTO.getDistance());
            }

            return RunDTOConverter.fromEntity(runRepository.save(startedRun));
        } else {
            throw new BusinessException("Run not found");
        }
    }

    public Page<RunDTO> getRunsForUserWithPagination(UUID userId,
                                                     OffsetDateTime fromDateTime,
                                                     OffsetDateTime toDateTime,
                                                     PageRequest page) {

        if (!userRepository.existsById(userId)) {
            throw new BusinessException("User not found");
        }

        if (fromDateTime != null && toDateTime != null) {
            return runRepository.findByUserIdAndStartDateTimeBetween(userId, fromDateTime, toDateTime, page)
                    .map(RunDTOConverter::fromEntity);
        } else if (fromDateTime != null) {
            return runRepository.findByUserIdAndStartDateTimeGreaterThanEqual(userId, fromDateTime, page)
                    .map(RunDTOConverter::fromEntity);
        } else if (toDateTime != null) {
            return runRepository.findByUserIdAndStartDateTimeLessThanEqual(userId, toDateTime, page)
                    .map(RunDTOConverter::fromEntity);
        } else {
            return runRepository.findByUserId(userId, page)
                    .map(RunDTOConverter::fromEntity);
        }
    }
}
