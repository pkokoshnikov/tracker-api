package org.pak.trackerapi.service;

import lombok.RequiredArgsConstructor;
import org.pak.trackerapi.converter.UserConverter;
import org.pak.trackerapi.error.BusinessException;
import org.pak.trackerapi.model.dto.UserCreateDTO;
import org.pak.trackerapi.model.dto.UserDTO;
import org.pak.trackerapi.repository.UserRepository;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public UserDTO getUserById(UUID userId) {
        return userRepository.findById(userId).map(UserConverter::fromEntity)
                .orElseThrow(() -> new BusinessException("User not found"));
    }

    public UserDTO createUser(UserCreateDTO user) {
        try {
            return UserConverter.fromEntity(userRepository.insert(UserConverter.toEntityForInsert(user)));
        } catch (DuplicateKeyException e) {
            throw new BusinessException("User already exists");
        }
    }

    public UserDTO updateUser(UUID userId, UserDTO updatedUser) {
        var user = userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new BusinessException("User not found");
        } else {
            return UserConverter.fromEntity(userRepository.save(UserConverter.toEntity(updatedUser)));
        }
    }

    public void deleteUser(UUID userId) {
        if (userRepository.existsById(userId)) {
            userRepository.deleteById(userId);
        } else {
            throw new BusinessException("User not found");
        }
    }

    public Page<UserDTO> getAllUsersWithPagination(PageRequest page) {
        return userRepository.findAll(page).map(UserConverter::fromEntity);
    }
}
