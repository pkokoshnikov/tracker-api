package org.pak.trackerapi.util;

import org.springframework.stereotype.Component;

@Component
public class Calculator {
    /**
     * Calculates the average speed from a list of distance-time pairs.
     *
     * @param distanceTimePairs The list of distance-time pairs.
     * @return The average speed in meters per second.
     */
    public double averageSpeed(DistanceTimePair[] distanceTimePairs) {
        if (distanceTimePairs == null || distanceTimePairs.length == 0) {
            return 0.0;
        }

        double totalDistance = 0.0;
        long totalSeconds = 0;

        for (DistanceTimePair pair : distanceTimePairs) {
            double distance = pair.distance();
            long time = pair.timeInSec();

            if (distance > 0 && time > 0) {
                totalDistance += distance;
                totalSeconds += time;
            }
        }

        double totalHours = (double) totalSeconds / 3600;
        double totalDistanceInKm = totalDistance / 1000;
        return (totalHours > 0) ? totalDistanceInKm / totalHours : 0.0;
    }

    /**
     * Calculates the distance between two geographical points using the Haversine formula in meters.
     *
     * @param startLatitude  The latitude of the starting point.
     * @param startLongitude The longitude of the starting point.
     * @param finishLatitude The latitude of the finishing point.
     * @param finishLongitude The longitude of the finishing point.
     * @return The distance between the two points in meters.
     */
    public long distance(double startLatitude,
                           double startLongitude,
                           double finishLatitude,
                           double finishLongitude) {
        final int R = 6371; // Earth's radius in kilometers

        // Convert latitude and longitude differences from degrees to radians
        double latDistance = Math.toRadians(finishLatitude - startLatitude);
        double lonDistance = Math.toRadians(finishLongitude - startLongitude);

        // Haversine formula
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(startLatitude)) * Math.cos(Math.toRadians(finishLatitude))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        // Distance in meters
        return (long) (R * c * 1000);
    }

    public record DistanceTimePair(double distance, long timeInSec) {
    }
}
