package org.pak.trackerapi.repository;

import org.pak.trackerapi.model.entity.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface UserRepository extends MongoRepository<UserEntity, UUID> {

}
