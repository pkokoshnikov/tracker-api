package org.pak.trackerapi.repository;

import org.pak.trackerapi.model.entity.RunEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Component
public interface RunRepository extends MongoRepository<RunEntity, UUID> {

    Page<RunEntity> findByUserId(UUID userId, Pageable pageable);

    List<RunEntity> findByUserId(UUID userId);

    Page<RunEntity> findByUserIdAndStartDateTimeBetween(UUID userId, OffsetDateTime fromDateTime, OffsetDateTime toDateTime,
                                                     Pageable pageable);

    List<RunEntity> findByUserIdAndStartDateTimeBetween(UUID userId, OffsetDateTime fromDateTime, OffsetDateTime toDateTime);

    RunEntity findByIdAndUserId(UUID id, UUID userId);

    Page<RunEntity> findByUserIdAndStartDateTimeGreaterThanEqual(UUID userId, OffsetDateTime fromDateTime, Pageable pageable);

    List<RunEntity> findByUserIdAndStartDateTimeGreaterThanEqual(UUID userId, OffsetDateTime fromDateTime);

    Page<RunEntity> findByUserIdAndStartDateTimeLessThanEqual(UUID userId, OffsetDateTime toDateTime, Pageable pageable);

    List<RunEntity> findByUserIdAndStartDateTimeLessThanEqual(UUID userId, OffsetDateTime toDateTime);
}
