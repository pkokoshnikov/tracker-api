package org.pak.trackerapi.converter;

import org.pak.trackerapi.model.dto.UserCreateDTO;
import org.pak.trackerapi.model.dto.UserDTO;
import org.pak.trackerapi.model.entity.UserEntity;

import java.util.Objects;
import java.util.UUID;

public class UserConverter {

    public static UserDTO fromEntity(UserEntity userEntity) {
        Objects.requireNonNull(userEntity, "userEntity must not be null");

        return new UserDTO(
                userEntity.getId(),
                userEntity.getFirstName(),
                userEntity.getLastName(),
                userEntity.getBirthDate(),
                userEntity.getSex()
        );
    }

    public static UserEntity toEntity(UserDTO userDTO) {
        Objects.requireNonNull(userDTO, "userDTO must not be null");

        return new UserEntity(userDTO.getUserId(),
                userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getBirthDate(),
                userDTO.getSex()
        );
    }

    public static UserEntity toEntityForInsert(UserCreateDTO userDTO) {
        Objects.requireNonNull(userDTO, "userDTO must not be null");

        return new UserEntity(
                UUID.randomUUID(),
                userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getBirthDate(),
                userDTO.getSex()
        );
    }
}
