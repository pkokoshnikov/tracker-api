package org.pak.trackerapi.converter;

import org.pak.trackerapi.model.dto.RunStartCreateDTO;
import org.pak.trackerapi.model.dto.RunStartDTO;
import org.pak.trackerapi.model.entity.RunEntity;

import java.time.ZoneOffset;
import java.util.Objects;
import java.util.UUID;

public class RunStartDTOConverter {

    public static RunStartDTO fromEntity(RunEntity runEntity) {
        Objects.requireNonNull(runEntity, "runEntity must not be null");

        return new RunStartDTO(
                runEntity.getId(),
                runEntity.getUserId(),
                runEntity.getStartLatitude(),
                runEntity.getStartLongitude(),
                runEntity.getStartDateTime().atZone(ZoneOffset.systemDefault()).toOffsetDateTime()
        );
    }

    public static RunEntity toEntity(RunStartCreateDTO runStartCreateDTO) {
        Objects.requireNonNull(runStartCreateDTO, "runStartDTO must not be null");

        return RunEntity.builder()
                .id(UUID.randomUUID())
                .userId(runStartCreateDTO.getUserId())
                .startDateTime(runStartCreateDTO.getStartDateTime().toInstant())
                .startLatitude(runStartCreateDTO.getStartLatitude())
                .startLongitude(runStartCreateDTO.getStartLongitude())
                .build();
    }
}
