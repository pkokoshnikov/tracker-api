package org.pak.trackerapi.converter;

import org.pak.trackerapi.model.dto.RunDTO;
import org.pak.trackerapi.model.entity.RunEntity;

import java.time.ZoneOffset;
import java.util.Objects;

public class RunDTOConverter {

    public static RunDTO fromEntity(RunEntity runEntity) {
        Objects.requireNonNull(runEntity, "runEntity must not be null");

        return new RunDTO(
                runEntity.getId(),
                runEntity.getUserId(),
                runEntity.getStartLatitude(),
                runEntity.getStartLongitude(),
                runEntity.getStartDateTime().atZone(ZoneOffset.systemDefault()).toOffsetDateTime(),
                runEntity.getFinishLatitude(),
                runEntity.getFinishLongitude(),
                runEntity.getFinishDateTime().atZone(ZoneOffset.systemDefault()).toOffsetDateTime(),
                runEntity.getDistance());
    }
}
