package org.pak.trackerapi.model.entity;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.UUID;

@Data
@Document(collection = Collections.RUNS)
@Builder
public class RunEntity {


    @Id
    private UUID id;

    @NotNull
    @Indexed
    private UUID userId;

    @NotNull
    private Double startLatitude;

    @NotNull
    private Double startLongitude;

    @NotNull
    private Instant startDateTime;

    private Double finishLatitude;

    private Double finishLongitude;

    private Instant finishDateTime;

    private Long distance;

}
