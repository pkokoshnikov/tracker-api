package org.pak.trackerapi.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class RunStartDTO extends RunStartCreateDTO {
    @Schema(description = "Run ID", example = "01c72031-f0ec-449d-b958-477c2596387b")
    @NotNull
    UUID id;

    public RunStartDTO(UUID id, UUID userId, Double startLatitude, Double startLongitude, OffsetDateTime startDateTime) {
        super(userId, startLatitude, startLongitude, startDateTime);
        this.id = id;
    }
}
