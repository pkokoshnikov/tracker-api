package org.pak.trackerapi.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Getter
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UserStatisticsDTO {
    @NotNull
    @Schema(description = "User ID", example = "0ad51bee-af5e-4947-9aca-901d49c6512f")
    UUID userId;

    @Schema(description = "Total number of runs")
    Long totalRuns;

    @Schema(description = "Total distance covered in meters")
    Long totalDistance;

    @Schema(description = "Average speed in meters per second")
    Double averageSpeed;
}
