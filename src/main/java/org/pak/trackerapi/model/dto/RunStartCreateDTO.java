package org.pak.trackerapi.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class RunStartCreateDTO {
    @NotNull
    @Schema(description = "User ID", example = "0ad51bee-af5e-4947-9aca-901d49c6512f")
    UUID userId;

    @NotNull
    @Schema(description = "Starting latitude", example = "37.7749")
    Double startLatitude;

    @NotNull
    @Schema(description = "Starting longitude", example = "-122.4194")
    Double startLongitude;

    @NotNull
    @Schema(description = "Start date and time", example = "2023-01-01T10:00:00Z")
    OffsetDateTime startDateTime;
}
