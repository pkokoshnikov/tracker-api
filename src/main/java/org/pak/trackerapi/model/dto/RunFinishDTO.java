package org.pak.trackerapi.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class RunFinishDTO {
    @NotNull
    @Schema(description = "Run ID", example = "01c72031-f0ec-449d-b958-477c2596387b")
    UUID id;

    @NotNull
    @Schema(description = "User ID", example = "0ad51bee-af5e-4947-9aca-901d49c6512f")
    UUID userId;

    @NotNull
    @Schema(description = "Finish latitude", example = "37.7749")
    Double finishLatitude;

    @NotNull
    @Schema(description = "Finish longitude", example = "-122.4194")
    Double finishLongitude;

    @NotNull
    @Schema(description = "Finish date and time", example = "2023-01-01T11:00:00Z")
    OffsetDateTime finishDateTime;

    @Schema(description = "Distance in meters", example = "5000.0")
    Long distance;
}
