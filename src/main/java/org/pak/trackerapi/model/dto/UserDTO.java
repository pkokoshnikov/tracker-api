package org.pak.trackerapi.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UserDTO extends UserCreateDTO {
    @NotNull
    @Schema(description = "User ID", example = "0ad51bee-af5e-4947-9aca-901d49c6512f")
    UUID userId;

    public UserDTO(UUID userId, String firstName, String lastName, LocalDate birthDate, String sex) {
        super(firstName, lastName, birthDate, sex);
        this.userId = userId;
    }
}
