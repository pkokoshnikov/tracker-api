package org.pak.trackerapi.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UserCreateDTO {

    @NotBlank
    @Schema(description = "User's first name", example = "John")
    @Size(min = 2, max = 50)
    String firstName;

    @NotBlank
    @Schema(description = "User's last name", example = "Doe")
    @Size(min = 2, max = 50)
    String lastName;

    @NotNull
    @Schema(description = "User's birth date", example = "1990-01-01")
    LocalDate birthDate;

    @NotBlank
    @Schema(description = "User's gender", example = "Male")
    @Size(min = 2, max = 10)
    String sex;
}
