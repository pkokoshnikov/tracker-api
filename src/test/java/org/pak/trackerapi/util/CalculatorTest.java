package org.pak.trackerapi.util;

import org.junit.jupiter.api.Test;
import org.pak.trackerapi.util.Calculator;

import static org.assertj.core.api.Assertions.*;

public class CalculatorTest {

    private final Calculator calculator = new Calculator();

    @Test
    public void testCalculateDistance() {
        // Given precise coordinates for two points (Moscow and Saint Petersburg)
        double moscowLatitude = 55.7558;
        double moscowLongitude = 37.6176;
        double stPetersburgLatitude = 59.9343;
        double stPetersburgLongitude = 30.3351;

        // When calculating the distance
        double distance = calculator.distance(
                moscowLatitude, moscowLongitude, stPetersburgLatitude, stPetersburgLongitude);

        // Then assert that the distance is ~ 634 km
        assertThat(distance).isEqualTo(633032); // Distance between Moscow and Saint Petersburg to ~ 634 km
    }


    @Test
    public void testCalculateAverageSpeed() {
        // Given a list of distance-time pairs
        Calculator.DistanceTimePair[] distanceTimePairs = {
                new Calculator.DistanceTimePair(1000.0, 600),
                new Calculator.DistanceTimePair(2000.0, 1200),
                new Calculator.DistanceTimePair(3000.0, 1800),
                new Calculator.DistanceTimePair(4000.0, 2400),
                new Calculator.DistanceTimePair(5000.0, 3000)
        };

        // When calculating the average speed
        double averageSpeed = calculator.averageSpeed(distanceTimePairs);

        // Then assert that the average speed is 6.0 km/h
        assertThat(averageSpeed).isEqualTo(6.0);
    }
}
