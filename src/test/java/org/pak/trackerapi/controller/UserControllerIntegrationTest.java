package org.pak.trackerapi.controller;

import org.junit.jupiter.api.Test;
import org.pak.trackerapi.model.dto.UserCreateDTO;
import org.pak.trackerapi.model.dto.UserDTO;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;


public class UserControllerIntegrationTest extends BaseControllerTest {

    @Test
    void testCreateUser() {
        var userDTO = new UserCreateDTO(
                "John",
                "Doe",
                LocalDate.parse("1990-01-01"),
                "Male");

        var responsePost = restTemplate.postForObject(url("/api/users"), requestEntity(userDTO), UserDTO.class);

        assertThat(responsePost.getUserId()).isNotNull();
        assertThat(responsePost.getFirstName()).isEqualTo(userDTO.getFirstName());
        assertThat(responsePost.getLastName()).isEqualTo(userDTO.getLastName());
        assertThat(responsePost.getBirthDate()).isEqualTo(userDTO.getBirthDate());
        assertThat(responsePost.getSex()).isEqualTo(userDTO.getSex());
    }

    @Test
    void testGetUser() {
        var userDTO = new UserCreateDTO(
                "John",
                "Doe",
                LocalDate.parse("1990-01-01"),
                "Male");

        var responsePost = restTemplate.postForObject(url("/api/users"), requestEntity(userDTO), UserDTO.class);

        var responseGet = restTemplate.getForObject(url("/api/users/" + responsePost.getUserId()), UserDTO.class);

        assertThat(responseGet.getUserId()).isEqualTo(responsePost.getUserId());
        assertThat(responseGet.getFirstName()).isEqualTo(userDTO.getFirstName());
        assertThat(responseGet.getLastName()).isEqualTo(userDTO.getLastName());
        assertThat(responseGet.getBirthDate()).isEqualTo(userDTO.getBirthDate());
        assertThat(responseGet.getSex()).isEqualTo(userDTO.getSex());
    }

    @Test
    void testUserValidation() {
        UserDTO userDTO = new UserDTO(null,
                null,
                "L",
                LocalDate.parse("1990-01-01"),
                "Male");

        var responsePost = restTemplate.postForObject(url("/api/users"), requestEntity(userDTO), String.class);

        assertThat(responsePost).contains("firstName: must not be blank", "lastName: size must be between 2 and 50");
    }

    /**
     * TODO: Add more tests
     */
}
