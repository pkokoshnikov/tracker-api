package org.pak.trackerapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
public class BaseControllerTest {

    @Autowired
    TestRestTemplate restTemplate;
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    ObjectMapper objectMapper;
    @LocalServerPort
    int port;

    static final MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:latest")
            .withExposedPorts(27017);

    static {
        mongoDBContainer.start();
    }

    @DynamicPropertySource
    static void registerProperties(DynamicPropertyRegistry registry) {
        registry.add("mongodb.container.port", () -> mongoDBContainer.getMappedPort(27017));
    }

    @NotNull
    String url(String x) {
        return "http://localhost:" + port + x;
    }

    @SneakyThrows
    @NotNull
    HttpEntity<String> requestEntity(Object requestBody) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(objectMapper.writeValueAsString(requestBody), headers);
    }

//    @AfterEach
//    void afterEach() {
//        mongoTemplate.dropCollection(Collections.RUNS);
//        mongoTemplate.dropCollection(Collections.USERS);
//    }

}
