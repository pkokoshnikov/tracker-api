package org.pak.trackerapi.controller;

import org.junit.jupiter.api.Test;
import org.pak.trackerapi.model.dto.*;
import org.springframework.http.HttpStatusCode;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class RunControllerIntegrationTest extends BaseControllerTest {

    @Test
    void testStartRun() {
        var userId = createUser().getUserId();

        var runStart = new RunStartCreateDTO(
                userId,
                123.456,
                789.012,
                OffsetDateTime.parse("2023-12-01T12:00:00Z"));

        String url = url("/api/runs/start");
        var response = restTemplate.postForObject(url, requestEntity(runStart), RunStartDTO.class);


        assertThat(response.getId()).isNotNull();
        assertThat(response.getUserId()).isEqualTo(runStart.getUserId());
        assertThat(response.getStartLatitude()).isEqualTo(runStart.getStartLatitude());
        assertThat(response.getStartLongitude()).isEqualTo(runStart.getStartLongitude());
    }

    @Test
    void testStopRun() {
        var userId = createUser().getUserId();

        var runStart = new RunStartCreateDTO(
                userId,
                123.456,
                789.012,
                OffsetDateTime.parse("2023-12-01T12:00:00Z"));

        String url = url("/api/runs/start");
        var response = restTemplate.postForObject(url, requestEntity(runStart), RunStartDTO.class);

        var runFinish = new RunFinishDTO(
                response.getId(),
                userId,
                51.456,
                312.012,
                OffsetDateTime.parse("2023-12-01T12:10:00Z"),
                1000L);

        String url2 = url("/api/runs/finish");
        var response2 = restTemplate.postForObject(url2, requestEntity(runFinish), RunFinishDTO.class);

        assertThat(response2.getId()).isNotNull();
        assertThat(response2.getUserId()).isEqualTo(runFinish.getUserId());
        assertThat(response2.getFinishLatitude()).isEqualTo(runFinish.getFinishLatitude());
        assertThat(response2.getFinishLongitude()).isEqualTo(runFinish.getFinishLongitude());
        assertThat(response2.getFinishDateTime()).isEqualTo(runFinish.getFinishDateTime());
        assertThat(response2.getDistance()).isEqualTo(runFinish.getDistance());
    }

    @Test
    void testFinishWithEmptyDistance() {
        var userId = createUser().getUserId();

        var runStart = new RunStartCreateDTO(
                userId,
                55.7558,
                37.6176,
                OffsetDateTime.parse("2023-12-01T12:00:00Z"));

        var response = restTemplate.postForObject(url("/api/runs/start"), requestEntity(runStart), RunStartDTO.class);

        var runFinish = new RunFinishDTO(
                response.getId(),
                userId,
                59.9343,
                30.3351,
                OffsetDateTime.parse("2023-12-01T12:10:00Z"),
                null);

        var response2 = restTemplate.postForObject(url("/api/runs/finish"), requestEntity(runFinish), RunFinishDTO.class);

        assertThat(response2.getId()).isNotNull();
        assertThat(response2.getUserId()).isEqualTo(runFinish.getUserId());
        assertThat(response2.getFinishLatitude()).isEqualTo(runFinish.getFinishLatitude());
        assertThat(response2.getFinishLongitude()).isEqualTo(runFinish.getFinishLongitude());
        assertThat(response2.getFinishDateTime()).isEqualTo(runFinish.getFinishDateTime());
        assertThat(response2.getDistance()).isEqualTo(633032);
    }

    @Test
    void testGetStatics() {
        var userId = createUser().getUserId();

        var runStart = new RunStartCreateDTO(
                userId,
                55.7558,
                37.6176,
                OffsetDateTime.parse("2023-12-01T12:00:00Z"));

        var response = restTemplate.postForObject(url("/api/runs/start"), requestEntity(runStart), RunStartDTO.class);

        var runFinish = new RunFinishDTO(
                response.getId(),
                userId,
                59.9343,
                30.3351,
                OffsetDateTime.parse("2023-12-01T12:10:00Z"),
                1000L);

        var response2 = restTemplate.postForObject(url("/api/runs/finish"), requestEntity(runFinish), RunFinishDTO.class);
        assertThat(response2.getId()).isNotNull();

        var response3 = restTemplate.getForObject(url("/api/runs/user/" + userId + "/statistics"), UserStatisticsDTO.class);

        assertThat(response3.getAverageSpeed()).isEqualTo(6.0);
        assertThat(response3.getTotalRuns()).isEqualTo(1);
        assertThat(response3.getTotalDistance()).isEqualTo(1000);
    }

    @Test
    void runFinishNotFound() {
        var userId = createUser().getUserId();

        var runFinish = new RunFinishDTO(
                UUID.randomUUID(),
                userId,
                59.9343,
                30.3351,
                OffsetDateTime.parse("2023-12-01T12:10:00Z"),
                1000L);

        var response2 = restTemplate.postForEntity(url("/api/runs/finish"), requestEntity(runFinish), String.class);
        assertThat(response2.getBody()).isEqualTo("Run not found");
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
    }

    @Test
    void userNotFound() {
        var runStart = new RunStartCreateDTO(
                UUID.randomUUID(),
                55.7558,
                37.6176,
                OffsetDateTime.parse("2023-12-01T12:00:00Z"));

        var response = restTemplate.postForEntity(url("/api/runs/start"), requestEntity(runStart), String.class);
        assertThat(response.getBody()).isEqualTo("User not found");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
    }

    private UserDTO createUser() {
        var userDTO = new UserCreateDTO(
                "John",
                "Doe",
                LocalDate.parse("1990-01-01"),
                "Male");

        return restTemplate.postForEntity(url("/api/users"), requestEntity(userDTO), UserDTO.class).getBody();
    }


    /**
     * TODO: Add more tests
     */
}
